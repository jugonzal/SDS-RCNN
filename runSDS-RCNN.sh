#!/bin/bash
#OAR -p gpu='YES' and dedicated='stars'
#OAR -l/gpunum=4, walltime=1
#OAR --notify mail:juan-diego.gonzales-zuniga@inria.fr

module load cuda/8.0 cudnn/5.1-cuda-8.0
export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:/usr/local/cuda/lib64"
args=("$@")
matlab2017a -nodisplay -nodesktop -r "run ${args[0]}"
echo Script ${args[0]} finished
